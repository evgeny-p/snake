import random, pygame, sys
from pygame.locals import *

FPS = 15
WINDOWWIDTH = 640
WINDOWHEIGHT = 480
CELLSIZE = 20
assert WINDOWWIDTH % CELLSIZE == 0, "Window width must be a multiple of cell size."
assert WINDOWHEIGHT % CELLSIZE == 0, "Window height must be a multiple of cell size."
CELLWIDTH = int(WINDOWWIDTH / CELLSIZE)
CELLHEIGHT = int(WINDOWHEIGHT / CELLSIZE)

#             R    G    B
WHITE     = (255, 255, 255)
BLACK     = (  0,   0,   0)
RED       = (255,   0,   0)
GREEN     = (  0, 255,   0)
DARKGREEN = (  0, 155,   0)
DARKGRAY  = ( 40,  40,  40)
BGCOLOR = BLACK

UP = 'up'
DOWN = 'down'
LEFT = 'left'
RIGHT = 'right'

HEAD = 0 # syntactic sugar: index of the worm's head


class Cell:
    def __init__(self, x, y):
        self.x = x
        self.y = y


def main():
    global FPSCLOCK, DISPLAYSURF, BASICFONT

    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    DISPLAYSURF = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
    BASICFONT = pygame.font.Font('freesansbold.ttf', 18)
    pygame.display.set_caption('Wormy')

    show_start_screen()
    while True:
        run_game()
        show_game_over_screen()


def run_game():
    # Set a random start point.
    startx = random.randint(5, CELLWIDTH - 6)
    starty = random.randint(5, CELLHEIGHT - 6)
    worm_coords = [
        Cell(startx, starty),
        Cell(startx - 1, starty),
        Cell(startx - 2, starty)]

    direction = RIGHT

    # Start the apple in a random place.
    apple = get_random_location()

    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                terminate()
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    terminate()
                direction = get_direction(event.key, direction)

        if worm_hit_edge(worm_coords) or worm_hit_self(worm_coords):
            return

        # check if worm has eaten an apply
        if worm_ate_apple(worm_coords, apple):
            # don't remove worm's tail segment
            apple = get_random_location() # set a new apple somewhere
        else:
            del worm_coords[-1] # remove worm's tail segment

        # move the worm by adding a segment in the direction it is moving
        new_head = get_worm_new_head(worm_coords, direction)

        worm_coords.insert(0, new_head)

        draw_frame(worm_coords, apple)

        FPSCLOCK.tick(FPS)


def draw_frame(worm_coords, apple):
    DISPLAYSURF.fill(BGCOLOR)
    draw_grid()
    draw_worm(worm_coords)
    draw_apple(apple)
    draw_score(len(worm_coords) - 3)
    pygame.display.update()


def get_direction(key, old_direction):
    direction = old_direction

    if (key == K_LEFT or key == K_a) and direction != RIGHT:
        direction = LEFT
    elif (key == K_RIGHT or key == K_d) and direction != LEFT:
        direction = RIGHT
    elif (key == K_UP or key == K_w) and direction != DOWN:
        direction = UP
    elif (key == K_DOWN or key == K_s) and direction != UP:
        direction = DOWN

    return direction


def worm_hit_edge(worm_coords):
    return (
        worm_coords[HEAD].x == -1 or
        worm_coords[HEAD].x == CELLWIDTH or
        worm_coords[HEAD].y == -1 or
        worm_coords[HEAD].y == CELLHEIGHT)


def worm_hit_self(worm_coords):
    for worm_body in worm_coords[1:]:
        if (worm_body.x == worm_coords[HEAD].x and
                worm_body.y == worm_coords[HEAD].y):
            return True

    return False


def worm_ate_apple(worm_coords, apple):
    return (
        worm_coords[HEAD].x == apple.x and
        worm_coords[HEAD].y == apple.y)


def get_worm_new_head(worm_coords, direction):
    if direction == UP:
        return Cell(worm_coords[HEAD].x, worm_coords[HEAD].y - 1)

    if direction == DOWN:
        return Cell(worm_coords[HEAD].x, worm_coords[HEAD].y + 1)

    if direction == LEFT:
        return Cell(worm_coords[HEAD].x - 1, worm_coords[HEAD].y)

    if direction == RIGHT:
        return Cell(worm_coords[HEAD].x + 1, worm_coords[HEAD].y)


def draw_press_key_msg():
    pressKeySurf = BASICFONT.render('Press a key to play.', True, DARKGRAY)
    pressKeyRect = pressKeySurf.get_rect()
    pressKeyRect.topleft = (WINDOWWIDTH - 200, WINDOWHEIGHT - 30)
    DISPLAYSURF.blit(pressKeySurf, pressKeyRect)


def check_for_key_press():
    if len(pygame.event.get(QUIT)) > 0:
        terminate()

    key_up_events = pygame.event.get(KEYUP)
    if len(key_up_events) == 0:
        return None
    if key_up_events[0].key == K_ESCAPE:
        terminate()
    return key_up_events[0].key


def show_start_screen():
    draw_press_key_msg()

    while True:
        if check_for_key_press():
            pygame.event.get() # clear event queue
            return
        pygame.display.update()
        FPSCLOCK.tick(FPS)


def terminate():
    pygame.quit()
    sys.exit()


def get_random_location():
    return Cell(
        random.randint(0, CELLWIDTH - 1),
        random.randint(0, CELLHEIGHT - 1))


def show_game_over_screen():
    gameOverFont = pygame.font.Font('freesansbold.ttf', 150)
    gameSurf = gameOverFont.render('Game', True, WHITE)
    overSurf = gameOverFont.render('Over', True, WHITE)
    gameRect = gameSurf.get_rect()
    overRect = overSurf.get_rect()
    gameRect.midtop = (WINDOWWIDTH / 2, 10)
    overRect.midtop = (WINDOWWIDTH / 2, gameRect.height + 10 + 25)

    DISPLAYSURF.blit(gameSurf, gameRect)
    DISPLAYSURF.blit(overSurf, overRect)
    draw_press_key_msg()
    pygame.display.update()
    pygame.time.wait(500)
    check_for_key_press() # clear out any key presses in the event queue

    while True:
        if check_for_key_press():
            pygame.event.get() # clear event queue
            return


def draw_score(score):
    scoreSurf = BASICFONT.render('Score: %s' % (score), True, WHITE)
    scoreRect = scoreSurf.get_rect()
    scoreRect.topleft = (WINDOWWIDTH - 120, 10)
    DISPLAYSURF.blit(scoreSurf, scoreRect)


def draw_worm(worm_coords):
    for coord in worm_coords:
        x = coord.x * CELLSIZE
        y = coord.y * CELLSIZE
        wormSegmentRect = pygame.Rect(x, y, CELLSIZE, CELLSIZE)
        pygame.draw.rect(DISPLAYSURF, DARKGREEN, wormSegmentRect)
        wormInnerSegmentRect = pygame.Rect(x + 4, y + 4, CELLSIZE - 8, CELLSIZE - 8)
        pygame.draw.rect(DISPLAYSURF, GREEN, wormInnerSegmentRect)


def draw_apple(coord):
    x = coord.x * CELLSIZE
    y = coord.y * CELLSIZE
    appleRect = pygame.Rect(x, y, CELLSIZE, CELLSIZE)
    pygame.draw.rect(DISPLAYSURF, RED, appleRect)


def draw_grid():
    for x in range(0, WINDOWWIDTH, CELLSIZE):
        pygame.draw.line(DISPLAYSURF, DARKGRAY, (x, 0), (x, WINDOWHEIGHT))
    for y in range(0, WINDOWHEIGHT, CELLSIZE):
        pygame.draw.line(DISPLAYSURF, DARKGRAY, (0, y), (WINDOWWIDTH, y))


if __name__ == '__main__':
    main()
