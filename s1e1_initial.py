# Нужно с чего-то начинать. Здесь уместен рассказ о том, что у всех игрушек
# есть одна общая часть — игровой цикл. Разберем, из каких действий он состоит,
# напишем скелет приложения, а потом будем заполнять пустоты логикой.
# Шаг первый — нужно создать окно.
#
# Этот код лучше дать как заготовку.

import pygame


WINDOW_WIDTH = 640
WINDOW_HEIGHT = 480

FPS = 15


def main():
    global FPS_CLOCK
    global DISPLAY

    pygame.init()
    FPS_CLOCK = pygame.time.Clock()
    DISPLAY = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
    pygame.display.set_caption('Wormy')

    while True:
        # Мы всегда будем начинать игру с начала. После проигрыша сразу
        # начинается следующая.
        run_game()


def run_game():
    # А вот это тот самый игровой цикл
    while True:
        # TODO: обработать пользовательский ввод
        # TODO: рассчитать физику: столкновения, новые позиции змейки и яблока
        # TODO: отрисовать кадр
        FPS_CLOCK.tick(FPS)


if __name__ == '__main__':
    main()
