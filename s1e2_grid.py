# Пора рисовать!
# Начнем с рисования сетки.

import pygame


WINDOW_WIDTH = 640
WINDOW_HEIGHT = 480

# Фиксируем размер ячейки
CELL_SIZE = 20

# Цвета
BG_COLOR = (0, 0, 0)
GRID_COLOR = (40, 40, 40)

FPS = 15


def main():
    global FPS_CLOCK
    global DISPLAY

    pygame.init()
    FPS_CLOCK = pygame.time.Clock()
    DISPLAY = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
    pygame.display.set_caption('Wormy')

    while True:
        # Мы всегда будем начинать игру с начала. После проигрыша сразу
        # начинается следующая.
        run_game()


def run_game():
    # А вот это тот самый игровой цикл
    while True:
        # TODO: обработать пользовательский ввод
        # TODO: рассчитать физику: столкновения, новые позиции змейки и яблока
        draw_frame()
        FPS_CLOCK.tick(FPS)


# Эта функция будет отвечать за рисование всего, что есть на экране:
# змейка, яблоко, сетка. Но пока - только сетка.
def draw_frame():
    DISPLAY.fill(BG_COLOR)
    draw_grid()
    pygame.display.update()


# Здесь знакомимся с рисованием линий. И больше они нигде не понадобятся. Упс.
# Requirements:
# * Циклы
# * Форма range с заданным шагом
def draw_grid():
    for x in range(0, WINDOW_WIDTH, CELL_SIZE):
        pygame.draw.line(DISPLAY, GRID_COLOR, (x, 0), (x, WINDOW_HEIGHT))
    for y in range(0, WINDOW_HEIGHT, CELL_SIZE):
        pygame.draw.line(DISPLAY, GRID_COLOR, (0, y), (WINDOW_WIDTH, y))


if __name__ == '__main__':
    main()
