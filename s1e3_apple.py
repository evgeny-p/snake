# Следующий логичный шаг после рисования линий - рисование прямоугольников.
# Ну или в нашем случае - квадратов. И у нас как раз есть такая задача -
# нам нужно научиться рисовать яблоки. Яблоко. Пока одно.

import pygame


WINDOW_WIDTH = 640
WINDOW_HEIGHT = 480

# Фиксируем размер ячейки
CELL_SIZE = 20

# Цвета
BG_COLOR = (0, 0, 0)
GRID_COLOR = (40, 40, 40)
# Яблоко будет самым красным!
APPLE_COLOR = (255, 0, 0)

FPS = 15


# Ячейка - весьма полезная структура для нашей задачи. Ну ок, формально - класс.
# Функционально - структура.
# Requirements:
# * Классы
# * __init__
class Cell:
    def __init__(self, x, y):
        self.x = x
        self.y = y


def main():
    global FPS_CLOCK
    global DISPLAY

    pygame.init()
    FPS_CLOCK = pygame.time.Clock()
    DISPLAY = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
    pygame.display.set_caption('Wormy')

    while True:
        # Мы всегда будем начинать игру с начала. После проигрыша сразу
        # начинается следующая.
        run_game()


def run_game():
    # Яблоко. Яблоко нужно не только для отрисовки, но и чтобы его есть.
    # Поэтому переменная должна быть здесь. Не вводя высокоуровневых абстракций,
    # делаем яблоко простой ячейкой в фиксированном месте поля.
    apple = Cell(20, 10)

    # А вот это тот самый игровой цикл
    while True:
        # TODO: обработать пользовательский ввод
        # TODO: рассчитать физику: столкновения, новые позиции змейки и яблока

        # Ой. А функция рисования кадра еще ничего не знает про яблоко.
        # Давайте поделимся с ней фруктом.
        draw_frame(apple)
        FPS_CLOCK.tick(FPS)


def draw_frame(apple):
    DISPLAY.fill(BG_COLOR)
    draw_grid()
    draw_apple(apple)
    pygame.display.update()


def draw_grid():
    for x in range(0, WINDOW_WIDTH, CELL_SIZE):
        pygame.draw.line(DISPLAY, GRID_COLOR, (x, 0), (x, WINDOW_HEIGHT))
    for y in range(0, WINDOW_HEIGHT, CELL_SIZE):
        pygame.draw.line(DISPLAY, GRID_COLOR, (0, y), (WINDOW_WIDTH, y))


# Здесь как будто бы ничего сложного, но есть момент, о котором нужно
# постоянно помнить. Нам удобно работать с сеткой. Но мы сами ее придумали.
# На экране тоже есть сетка, но гораздо более мелкая - пиксельная. Надо
# пересчитывать координаты нашей сетки в координаты пиксельной.
def draw_apple(apple):
    draw_cell(apple, APPLE_COLOR)


# А эта вспомогательная функция нам еще пригодится.
def draw_cell(cell, color):
    x = cell.x * CELL_SIZE
    y = cell.y * CELL_SIZE
    apple_rect = pygame.Rect(x, y, CELL_SIZE, CELL_SIZE)
    pygame.draw.rect(DISPLAY, color, apple_rect)


if __name__ == '__main__':
    main()
