# Наша змея попала в игровой ад. Она ползет за яблоком, не может его съесть,
# врезается в стену, погибает, воскресает, и все повторяется снова.
# Если так будет продолжаться и дальше, за нами придет гринпис и начнет
# читать скучные лекции о жестоком обращении с животными. Давайте позволим
# нашей змее все-таки съесть яблоко на ее пути.


import pygame
import random


WINDOW_WIDTH = 640
WINDOW_HEIGHT = 480

# Фиксируем размер ячейки
CELL_SIZE = 20

CELL_WIDTH = int(WINDOW_WIDTH / CELL_SIZE)
CELL_HEIGHT = int(WINDOW_HEIGHT / CELL_SIZE)

# Цвета
BG_COLOR = (0, 0, 0)
GRID_COLOR = (40, 40, 40)
APPLE_COLOR = (255, 0, 0)
APPLE_OUTER_COLOR = (155, 0, 0)
SNAKE_COLOR = (0, 255, 0)
SNAKE_OUTER_COLOR = (0, 155, 0)

FPS = 15

UP = 'up'
DOWN = 'down'
LEFT = 'left'
RIGHT = 'right'

HEAD = 0


class Cell:
    def __init__(self, x, y):
        self.x = x
        self.y = y


def main():
    global FPS_CLOCK
    global DISPLAY

    pygame.init()
    FPS_CLOCK = pygame.time.Clock()
    DISPLAY = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
    pygame.display.set_caption('Wormy')

    while True:
        # Мы всегда будем начинать игру с начала. После проигрыша сразу
        # начинается следующая.
        run_game()


def run_game():
    apple = Cell(20, 10)

    snake = [
        Cell(5, 10),
        Cell(4, 10),
        Cell(3, 10)]

    direction = RIGHT

    # А вот это тот самый игровой цикл
    while True:
        # TODO: обработать пользовательский ввод

        if snake_hit_edge(snake):
            return

        if snake_hit_apple(snake, apple):
            snake_grow(snake)
            # Одно яблоко съели, нужно разместить новое.
            apple = new_apple()

        move_snake(snake, direction)

        draw_frame(snake, apple)
        FPS_CLOCK.tick(FPS)


def snake_hit_edge(snake):
    return (
        snake[HEAD].x == -1 or
        snake[HEAD].x == CELL_WIDTH or
        snake[HEAD].y == -1 or
        snake[HEAD].y == CELL_HEIGHT)


def snake_hit_apple(snake, apple):
    return snake[HEAD].x == apple.x and snake[HEAD].y == apple.y


# Расти змея будет вот таким хитрым образом. И на занятии нужно объяснить,
# как и почему это работает.
def snake_grow(snake):
    snake.append(snake[-1])


def move_snake(snake, direction):
    new_head = get_snake_new_head(snake, direction)
    snake.insert(0, new_head)
    del snake[-1]


def get_snake_new_head(snake, direction):
    if direction == UP:
        return Cell(snake[HEAD].x, snake[HEAD].y - 1)

    if direction == DOWN:
        return Cell(snake[HEAD].x, snake[HEAD].y + 1)

    if direction == LEFT:
        return Cell(snake[HEAD].x - 1, snake[HEAD].y)

    if direction == RIGHT:
        return Cell(snake[HEAD].x + 1, snake[HEAD].y)


# Requirement: псевдослучайные числа
def new_apple():
    return Cell(
        random.randint(0, CELL_WIDTH - 1),
        random.randint(0, CELL_HEIGHT - 1))


def draw_frame(snake, apple):
    DISPLAY.fill(BG_COLOR)
    draw_grid()
    draw_snake(snake)
    draw_apple(apple)
    pygame.display.update()


def draw_grid():
    for x in range(0, WINDOW_WIDTH, CELL_SIZE):
        pygame.draw.line(DISPLAY, GRID_COLOR, (x, 0), (x, WINDOW_HEIGHT))
    for y in range(0, WINDOW_HEIGHT, CELL_SIZE):
        pygame.draw.line(DISPLAY, GRID_COLOR, (0, y), (WINDOW_WIDTH, y))


def draw_apple(apple):
    draw_cell(apple, APPLE_OUTER_COLOR, APPLE_COLOR)


def draw_snake(snake):
    for cell in snake:
        draw_cell(cell, SNAKE_OUTER_COLOR, SNAKE_COLOR)


def draw_cell(cell, outer_color, inner_color):
    x = cell.x * CELL_SIZE
    y = cell.y * CELL_SIZE

    rect = pygame.Rect(x, y, CELL_SIZE, CELL_SIZE)
    pygame.draw.rect(DISPLAY, outer_color, rect)

    inner_rect = pygame.Rect(x + 4, y + 4, CELL_SIZE - 8, CELL_SIZE - 8)
    pygame.draw.rect(DISPLAY, inner_color, inner_rect)


if __name__ == '__main__':
    main()
